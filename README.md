# Py-Snake!

## Overview

Snake! A retro game based on Blockade (1976 Gremlin). The player controls a snake (represented by a pink line) using the arrow keys on the keyboard.

The object of the game is to eat apples (red dots) by running into them with the head of the snake. When the player eats an apple, they get a point and the snake grows in length. Be careful not to let the snake eat itself!

Py-Snake was my second unguided project I made in Python. This project taught me more about drawing objects to the screen and manipulating timing of the main game loop.

## Features
* Keyboard input
* Color graphics

## Installation Instructions
Currently, I have made no attempt to create a standalone executable, so the game can only be run by installing Python and running py-snake.py from the command line.

## Requirements
* Python 3.8
* Pygame 2.0.1
(The above versions listed were the last to be tested and known working with the project.)

## Contributors
* VGpunx - Primary contributor
* Taylor-MadeAK - Code review