from src.game import *


pygame.init()
pygame.font.init()

if __name__ == "__main__":
    game = Game()
    game.run()
    pygame.quit()
