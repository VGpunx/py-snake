# The size of the entire game window
DISPLAY_SIZE = (528, 640)

# Size of the Playfield
PLAYFIELD_SIZE = (528, 560)

# Size of Scoreboard
SCOREBOARD_SIZE = (528, (DISPLAY_SIZE[1] - PLAYFIELD_SIZE[1]))

# Snake movement speed
MOVE_SPEED = 4
