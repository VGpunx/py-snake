import pygame, math
from src.snake_segment import *


class Snake(pygame.sprite.Group):
    def __init__(self, start_position, *sprites):
        """
        :param start_position: tuple
        """
        super().__init__(*sprites)

        # Direction is based on the following:
        #   - 0 degrees is right
        #   - 90 is up
        #   - 180 is left
        #   - 270 is down
        # TODO: there's nothing preventing this variable from going outside of the 0-360 degree range
        self.direction = 90
        self.max_length = 4
        self.pos = start_position

        # Instantiate object for snake segments
        # populate the initial snake with 4 segments
        # these will initially be all stacked up on the same
        # position vector
        self.add(SnakeSegment(self.pos))
        self.spr_width = self.sprites()[0].rect.width

    def turn(self, direction):
        """
        Takes "cw" (clockwise) or "ccw" (counter-clockwise) for arguments.
        :param direction: string
        :return: None
        """

        # this constrains turns to 90 degrees, thereby preventing direction reversals
        if direction.lower() == "cw": direction = -90
        elif direction.lower() == "ccw": direction = 90

        new_heading = self.direction + direction

        # this wraps values exceeding 359 degrees
        if new_heading >= 360:
            new_heading = new_heading % 360

        # this wraps the value if it becomes negative
        if new_heading < 0:
            new_heading += 360

        self.direction = new_heading

    def move(self):
        """
        Moves the snake by appending a segment as the new head with a new position and removing the last one.
        In this case, the "head" will be the final segment in the spritelist, and the tail will be the first segment.
        :return:
        """

        # determine which way to move
        cur_head_pos = self.sprites()[len(self) - 1].position

        if self.direction == 0:
            # move right
            new_head_pos = (cur_head_pos[0] + self.spr_width, cur_head_pos[1])
        elif self.direction == 90:
            # move up
            new_head_pos = (cur_head_pos[0], cur_head_pos[1] - self.spr_width)
        elif self.direction == 180:
            # move left
            new_head_pos = (cur_head_pos[0] - self.spr_width, cur_head_pos[1])
        elif self.direction == 270:
            # move down
            new_head_pos = (cur_head_pos[0], cur_head_pos[1] + self.spr_width)

        # add the new head and update position
        # this will throw an exception if new_head_pos is still None, and that's intentional
        self.pos = new_head_pos
        self.add(SnakeSegment(new_head_pos))

        # destroy the tail segment
        if len(self.sprites()) > self.max_length:
            self.sprites().pop(0).kill()

    def reset(self, position):
        self.__init__(position)