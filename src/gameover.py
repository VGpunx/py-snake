import pygame
import os


class GameOverScreen:
    def __init__(self, size):
        """
        This class will contain the game over screen
        :param size: tuple  - should be proportionate to the SCREEN_SIZE
        :param groups: Sprite.Group
        """
        self.size = size
        self.image = pygame.Surface(self.size)
        self.image.fill(pygame.Color('BLACK'))
        self.rect = self.image.get_rect()

        self.centerfield = (self.size[0] / 2, self.size[1] / 2)

        self.position = (0, 0)

        # The test text object
        self.go_font = pygame.font.Font(None, 36)
        self.text = self.go_font.render("GAME OVER", True, pygame.Color('BLUE'))
        self.text_rect = self.text.get_rect()
        self.text_pos = (((self.size[0] / 2) - (self.text_rect.width / 2)), ((self.size[1] / 2) - (self.text_rect.height / 2)))

        self.inst_font = pygame.font.Font(None, 20)
        self.instructions = self.inst_font.render("Press [ESC] to quit, or [SPACEBAR] to try again.", True, pygame.Color('WHITE'))
        self.inst_rect = self.instructions.get_rect()
        self.inst_pos = (((self.size[0] / 2) - (self.inst_rect.width / 2)), ((self.size[1] / 2 + 50) - (self.inst_rect.height / 2)))

    def draw(self, surface):
        surface.blit(self.image, self.position)
        surface.blit(self.text, self.text_pos)
        surface.blit(self.instructions, self.inst_pos)
