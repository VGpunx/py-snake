import pygame


class SnakeSegment(pygame.sprite.Sprite):
    def __init__(self, position, *groups):
        pygame.sprite.Sprite.__init__(self)
        super().__init__(*groups)
        self.__size__ = (16, 16)
        self.image = pygame.Surface(self.__size__)
        self.image.fill(pygame.Color('LIGHTPINK'))
        self.rect = self.image.get_rect()
        self.position = position  # used for tracking
        self.rect.topleft = self.position
