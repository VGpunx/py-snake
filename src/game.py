import pygame
from pygame.locals import *
from src.constants import *
from src.playfield import *
from src.controls import *
from src.scoreboard import *
from src.gameover import *
from src.title_screen import *


class Game:
    def __init__(self):
        # Create the screen object
        self.screen = pygame.display.set_mode(DISPLAY_SIZE)

        # Set the title bar caption
        pygame.display.set_caption("Snake!")

        # Main game loop exit conditional and title screen conditional
        self.done = False
        self.title_screen_flag = True

        # Used to manage how fast the screen updates
        self.clock = pygame.time.Clock()

        # Instantiate objects
        self.playfield = Playfield(PLAYFIELD_SIZE, (0, (DISPLAY_SIZE[1] - PLAYFIELD_SIZE[1])))
        self.scoreboard = Scoreboard(SCOREBOARD_SIZE, (0, 0))
        self.controls = Controls(self.playfield.player_snake)
        self.game_over_screen = GameOverScreen(DISPLAY_SIZE)
        self.title_screen = TitleScreen(DISPLAY_SIZE)

        # Snake movement control
        # TODO: Possibly move this code to a different method
        self.SN_MOVE_EVENT = pygame.USEREVENT + 3
        pygame.time.set_timer(self.SN_MOVE_EVENT, int(1000 / MOVE_SPEED))

    def run(self):
        # Snake turning flag - Ensures the snake can only turn once per "move"
        f_snake_turn = True

        # Main game loop
        while not self.done:
            # Control event loop
            self.controls.process_controls()

            # Main event loop
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.done = True

                # Respond to control events
                if event.type == self.controls.ccw and f_snake_turn:
                    self.playfield.player_snake.turn("ccw")
                    f_snake_turn = False
                if event.type == self.controls.cw and f_snake_turn:
                    self.playfield.player_snake.turn("cw")
                    f_snake_turn = False

                if event.type == self.SN_MOVE_EVENT:
                    self.playfield.player_snake.move()
                    f_snake_turn = True

                if event.type == self.playfield.score_point:
                    self.scoreboard.point()

                if self.playfield.game_over:
                    if event.type == self.controls.new_game:
                        self.scoreboard.reset()
                        self.playfield.reset()

                if self.title_screen_flag:
                    if event.type == self.controls.new_game:
                        self.scoreboard.reset()
                        self.playfield.reset()
                        self.title_screen_flag = False

            if self.title_screen_flag:
                self.screen.fill(pygame.Color('BLACK'))
                self.title_screen.draw(self.screen)

            if self.playfield.game_over:
                self.screen.fill(pygame.Color('BLACK'))
                self.game_over_screen.draw(self.screen)

            if not self.playfield.game_over and not self.title_screen_flag:
                # Game logic
                self.playfield.process_powerup()
                self.playfield.process_collision()

                # Clear the screen (blit the background image)
                self.screen.fill(pygame.Color("GREY"))

                # Draw all sprites and objects
                self.playfield.draw(self.screen)
                self.scoreboard.draw(self.screen)

            # Update the screen with what was drawn
            pygame.display.flip()

            # FPS = 2
            self.clock.tick(60)
