import pygame


class Pickup(pygame.sprite.DirtySprite):
    def __init__(self, position, *groups):
        """
        Pickup class for power up items (like apples)
        """
        super().__init__(*groups)
        self._size = (16, 16)
        self.image = pygame.Surface(self._size)
        self.image.fill(pygame.Color('RED'))
        self.rect = self.image.get_rect()
        self.position = position
        self.rect.topleft = self.position
