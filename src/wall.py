import pygame


class Wall(pygame.sprite.Sprite):
    def __init__(self, position, *groups):
        pygame.sprite.Sprite.__init__(self)
        super().__init__(*groups)
        self.__size__ = (16, 16)
        self.position = position
        self.image = pygame.Surface(self.__size__)
        self.image.fill(pygame.Color('BLUE'))
        self.rect = self.image.get_rect()
        self.rect.topleft = position

        # TODO: Walls should generate based on what section of wall they are.

