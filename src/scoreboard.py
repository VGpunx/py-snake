import pygame
from src.scoreboardnumeral import *


class Scoreboard:
    def __init__(self, size, position, *groups):
        """
        Scoreboard class to display four-digit score at the top of the screen.
        :param size: tuple
        :param position: tuple
        :param groups: Sprite.Group
        """
        super().__init__(*groups)
        self.size = size
        self.position = position
        self.image = pygame.Surface(self.size)
        self.rect = self.image.get_rect()

        # Create image and background and fill with black
        self.image = pygame.Surface(size)
        self.background = pygame.Surface(size)
        self.background.fill(pygame.Color('BLACK'))

        # Numeral size
        self.numeral_size = (self.size[1] - 20)

        # Instantiate numerals
        self.numerals = pygame.sprite.Group()
        self.score_thousands = ScoreboardNumeral(self.numeral_size, 'WHITE', 'BLACK', self.numerals)
        self.score_hundreds = ScoreboardNumeral(self.numeral_size, 'WHITE', 'BLACK', self.numerals)
        self.score_tens = ScoreboardNumeral(self.numeral_size, 'WHITE', 'BLACK', self.numerals)
        self.score_ones = ScoreboardNumeral(self.numeral_size, 'WHITE', 'BLACK', self.numerals)

        # Set numeral position on scoreboard using .rect.topleft
        self.digit_offset = self.score_thousands.get_width() + self.score_thousands.get_stroke_width()
        self.digit_space = 10
        self.score_thousands.rect.topleft = (10, 10)
        self.score_hundreds.rect.topleft = (self.digit_offset + self.digit_space, 10)
        self.score_tens.rect.topleft = ((self.digit_offset * 2 + self.digit_space), 10)
        self.score_ones.rect.topleft = ((self.digit_offset * 3 + self.digit_space), 10)

        # Set the numeral values using .set_value(x)
        self.score_thousands.set_value(0)
        self.score_hundreds.set_value(0)
        self.score_tens.set_value(0)
        self.score_ones.set_value(0)

    def point(self):
        # Check to ensure the single-digit bounds are not exceeded
        if self.score_thousands.value == 9 and self.score_hundreds.value == 9 and self.score_tens.value == 9 and self.score_thousands.value == 9:
            self.score_thousands.set_value(0)
            self.score_hundreds.set_value(0)
            self.score_tens.set_value(0)
            self.score_ones.set_value(0)

        elif self.score_hundreds.value == 9 and self.score_tens.value == 9 and self.score_ones.value == 9:
            self.score_ones.set_value(0)
            self.score_tens.set_value(0)
            self.score_hundreds.set_value(0)
            self.score_thousands.set_value(self.score_thousands.value + 1)

        elif self.score_ones.value == 9 and self.score_tens.value == 9:
            self.score_ones.set_value(0)
            self.score_tens.set_value(0)
            self.score_hundreds.set_value(self.score_hundreds.value + 1)

        elif self.score_ones.value == 9:
            self.score_ones.set_value(0)
            self.score_tens.set_value(self.score_tens.value + 1)

        else:
            self.score_ones.set_value(self.score_ones.value + 1)

    def draw(self, surface):
        self.image.fill(pygame.Color('BLACK'))
        self.numerals.draw(self.image)
        surface.blit(self.image, self.position)

    def update(self):
        super().update()

    def reset(self):
        self.score_thousands.set_value(0)
        self.score_hundreds.set_value(0)
        self.score_tens.set_value(0)
        self.score_ones.set_value(0)