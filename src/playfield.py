import pygame
from src.wall import *
from src.snake import *
from src.pickup import *
from random import *
from src.constants import *


class Playfield:
    def __init__(self, size, position):
        """
        size - tuple
        position - tuple
        """
        self.size = size
        self.position = position

        # Create image and background and fill with black
        self.image = pygame.Surface(size)
        self.background = pygame.Surface(size)
        self.background.fill(pygame.Color('BLACK'))

        self.rect = self.image.get_rect()

        # Blit the image to the backround
        self.image.blit(self.background, self.rect.topleft)

        # Instantiate sprite groups
        self.outer_walls = pygame.sprite.Group()    # Outer walls group for drawing the outer walls specifically
        self.walls = pygame.sprite.Group()          # Super group containing all walls for collision detection
        self.powerups = pygame.sprite.LayeredDirty()

        # Instantiate custom event for scoring a point
        self.score_point = pygame.USEREVENT + 4

        for i in range(0, self.size[0], 16):
            self.outer_walls.add(Wall((i, 0)))
            self.outer_walls.add(Wall((i, self.size[1]-16)))

        for i in range(16, self.size[1], 16):
            self.outer_walls.add(Wall((0, i)))
            self.outer_walls.add(Wall(((self.size[0]-16), i)))

        # Add outer walls to self.walls
        self.walls.add(self.outer_walls)

        # Game over flag
        self.game_over = False

        # Instantiate game objects
        self.player_snake = Snake((528 - 64, 560 - 64))
        self.apple = Pickup((192, 192), self.powerups)

        #  Set playfield position

    def draw(self, surface):
        self.outer_walls.clear(self.image, self.background)
        self.player_snake.clear(self.image, self.background)

        self.outer_walls.draw(self.image)
        self.player_snake.draw(self.image)
        self.powerups.draw(self.image)

        surface.blit(self.image, self.position)

    def process_collision(self):
        # Process collision against walls
        if len(pygame.sprite.groupcollide(self.player_snake, self.walls, True, False)):
            self.game_over = True

        # Process collision against the snake
        sn_head = self.player_snake.sprites()[-1]   # -1 index should grab the last sprite in the list

        for sprite in self.player_snake.sprites()[0:-2]:
            if pygame.sprite.collide_rect(sn_head, sprite):
                self.game_over = True

    def new_powerup(self):
        new_x = randrange(16, PLAYFIELD_SIZE[0] - 32, 16)
        new_y = randrange(16, PLAYFIELD_SIZE[1] - 32, 16)
        new_apple = Pickup((new_x, new_y))
        self.powerups.add(new_apple)

    def process_powerup(self):
        # Process collision against pickups/powerups
        if len(pygame.sprite.groupcollide(self.player_snake, self.powerups, False, True)):
            pygame.event.post(pygame.event.Event(self.score_point))
            self.player_snake.max_length += 1
            self.powerups.clear(self.image, self.background)
            self.new_powerup()

    def reset(self):
        self.player_snake.clear(self.image, self.background)
        self.player_snake.reset((528 - 64, 560 - 64))
        self.powerups.empty()
        self.powerups.clear(self.image, self.background)
        self.new_powerup()
        self.game_over = False
