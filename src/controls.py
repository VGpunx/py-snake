import pygame, pygame.locals


class Controls:
    def __init__(self, snake):
        self.cw = pygame.USEREVENT + 1
        self.ccw = pygame.USEREVENT + 2
        self.new_game = pygame.USEREVENT + 5
        self.snake = snake

        self._valid_controls = (
            pygame.K_UP,
            pygame.K_DOWN,
            pygame.K_LEFT,
            pygame.K_RIGHT,
            pygame.K_w,
            pygame.K_s,
            pygame.K_a,
            pygame.K_d,
            pygame.K_ESCAPE,
            pygame.K_SPACE
        )

    def process_controls(self):
        # Direction is based on the following:
        #   - 0 degrees is right
        #   - 90 is up
        #   - 180 is left
        #   - 270 is down
        dir = self.snake.direction

        # Flag to ensure processing only occurs when a valid key press is detected.
        for event in pygame.event.get(pygame.KEYDOWN):
            if event.key not in self._valid_controls:
                continue

            if event.key == pygame.K_ESCAPE:
                pygame.event.post(pygame.event.Event(pygame.QUIT))

            if event.key == pygame.K_SPACE:
                pygame.event.post(pygame.event.Event(self.new_game))

            # Controls are based on direction
            if dir == 0:
                if event.key == pygame.K_UP or event.key == pygame.K_w:
                    pygame.event.post(pygame.event.Event(self.ccw))
                    return
                elif event.key == pygame.K_DOWN or event.key == pygame.K_s:
                    pygame.event.post(pygame.event.Event(self.cw))
                    return
            if dir == 90:
                if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                    pygame.event.post(pygame.event.Event(self.cw))
                    return
                elif event.key == pygame.K_LEFT or event.key == pygame.K_a:
                    pygame.event.post(pygame.event.Event(self.ccw))
                    return
            if dir == 180:
                if event.key == pygame.K_UP or event.key == pygame.K_w:
                    pygame.event.post(pygame.event.Event(self.cw))
                    return
                elif event.key == pygame.K_DOWN or event.key == pygame.K_s:
                    pygame.event.post(pygame.event.Event(self.ccw))
                    return
            if dir == 270:
                if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                    pygame.event.post(pygame.event.Event(self.ccw))
                    return
                elif event.key == pygame.K_LEFT or event.key == pygame.K_a:
                    pygame.event.post(pygame.event.Event(self.cw))
                    return
